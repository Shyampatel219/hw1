package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>  {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    //(SP)
    private final StringProperty email;

    /**
     * Constructor initializes the TA name
     */
    public TeachingAssistant(String initName) {
        name = new SimpleStringProperty(initName);
        //(SP)
        email = new SimpleStringProperty();
    }
    //(SP)
     public TeachingAssistant(String initName, String initEmail) {
        name = new SimpleStringProperty(initName);
        //(SP)
        email = new SimpleStringProperty(initEmail);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }
    //(SP)
    public String getEmail(){
        return email.get();
    }
    public void setRmail(String initEmail){
        email.set(initEmail);
    }

    
    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }
}